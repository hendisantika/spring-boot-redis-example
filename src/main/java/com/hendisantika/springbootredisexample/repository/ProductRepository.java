package com.hendisantika.springbootredisexample.repository;

import com.hendisantika.springbootredisexample.domain.Product;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-redis-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/03/18
 * Time: 20.47
 * To change this template use File | Settings | File Templates.
 */
public interface ProductRepository extends CrudRepository<Product, String> {
}
