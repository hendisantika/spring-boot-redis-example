package com.hendisantika.springbootredisexample.service;

import com.hendisantika.springbootredisexample.command.ProductForm;
import com.hendisantika.springbootredisexample.domain.Product;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-redis-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/03/18
 * Time: 20.49
 * To change this template use File | Settings | File Templates.
 */
public interface ProductService {
    List<Product> listAll();

    Product getById(String id);

    Product saveOrUpdate(Product product);

    void delete(String id);

    Product saveOrUpdateProductForm(ProductForm productForm);
}
